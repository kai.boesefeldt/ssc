# Siri's Silly Cousin - Top Level Software Architecture

## General Structure

```plantuml
@startuml

title Siri's Silly Cousin - Top Level Software Architecture

left to right direction

interface InputTextProvider
interface ActionClassifier
interface ActionExecutor
interface OutputConsumer
interface AI
interface AIAccess

component SirisSillyCousin
SirisSillyCousin -u-> InputTextProvider
SirisSillyCousin --> ActionClassifier
SirisSillyCousin -u-> ActionExecutor
SirisSillyCousin -u-> OutputConsumer
SirisSillyCousin --> AI
SirisSillyCousin - AIAccess

component AIEngine
AIEngine -l- AI

component IO
InputTextProvider -u- IO
OutputConsumer -u- IO

component Classifier
Classifier - ActionClassifier
Classifier --> AIAccess

component Action
Action -- ActionExecutor
Action -u-> AIAccess

actor User
User --> IO : Voice, Text

actor OtherUsers
OtherUsers --> IO : Messages

actor TheWeb
TheWeb --> IO : Public Content 

@enduml
```
Input comes from various sources into the system, and is translated into plain text. This is retrieved via the `InputTextProvider` interface and attached to an input category,  (depending on the source of input, e.g. `SpeechConverter`, `TextTerminal`, `MessageReceiver`, `MessageSender`, `InformationRetriever`, `EventReceiver`.

The classifier's task is to analyze the input and figure out, which actions would be useful as reaction to that input. So we take the appropriate `ActionClassifier` implementation matching the input category, and let it determine the `ActionClassificationID` for that input. There may be specialized classifiers for each type of input (e.g. `CommandClassifier`, `MessageClassifier`, `PublicInfoClassifier`)

From this `ActionClassificationID`, whe find the appropriate `ActionExecutor` instances for that input. Each of these instances has special capabilities to further process the input and generate some sort of output. Actions may be e.g. `Ignorer`, `DialogueReplier`, `MessageSender`, `AppointmentMaker`, `PaymentMaker`, `EventScheduler`, `WebsiteOpener`, `Explainer`.

The output is in turn sent to external devices or apps to trigger the appropriate actions.

Several AI engines are used for different purposes, e.g. for first classification of the input text. It follows a detailed handling of the input in the appropriate action executor.

## Processing of Input

```plantuml
actor External 
participant InputTextProvider [
    InputTextProvider
    ----
    Various input
    methods
]
External -> InputTextProvider : Input\nAny Format
participant ActionClassifier [
    ActionClassifier
    ----
    Specific for the
    type of input
]
InputTextProvider -> ActionClassifier : Text Input
participant AIAccess [
  =AIAccess
  ----
  Different AI engines
  for different purposes
]
ActionClassifier -> AIAccess: Text Input
ActionClassifier <-- AIAccess : Content Classification
participant ActionExecutor [
    ActionExecutor
    ----
    Depends on content
    classification
]
ActionClassifier -> ActionExecutor : Classified Text Input
ActionExecutor -> AIAccess : AI prompt with\ntext Input
ActionExecutor <-- AIAccess : AI Response
participant OutputConsumer [
    OutputConsumer
    ----
    Depends on
    output type
]
ActionExecutor -> OutputConsumer : Output
OutputConsumer --> External : Response\nAny Format 
```

## Natural Language Data Querying

A language model of an AI can hardly be perfectly trained for all possible contents of a large and frequently changing content base (such as the Web). In this section, we outline how this language model can nevertheless be used for natural language queries of current information from such a collection of text resources.

```plantuml
actor User
participant TextInput [
    Text Input / Output
]
User -> TextInput : Request in\nnatural language

participant QueryProcessor [
    Natural Language Query Processor 
]
TextInput -> QueryProcessor : <NATURAL_LANGUAGE_REQUEST>
|||
participant AI [
    AI Language Model
    ----
    E.g. ChatGPT
]
QueryProcessor -> AI : ask("Extract keywords from\nthe following text:\n<NATURAL_LANGUAGE_REQUEST>")
QueryProcessor <-- AI : <KEYWORDS>
|||
participant SearchEngine [
    SearchEngine
    ----
    E.g. Web Search Engine
    or Full Text Search Service
]
QueryProcessor -> SearchEngine : search(<KEYWORDS>)
QueryProcessor <-- SearchEngine : <SEARCH_RESULTS>
|||
collections Resources
loop for <CURR_RESULT> in <SEARCH_RESULTS 0..n>
|||
QueryProcessor -> Resources : getContent(<CURR_RESULT>)
QueryProcessor <-- Resources : Normalize and add downloaded web resource to <CONTENT>
|||
end
|||
QueryProcessor -> AI : ask("Summarize the following content: <CONTENT>")
QueryProcessor <-- AI : <CONTENT_SUMMARY>
TextInput <-- QueryProcessor : <CONTENT_SUMMARY>
|||
```

A user makes a textual natural language query (be it via a keyboard, voice input, handwritten or otherwise). This query is pre-processed by the `Natural Language Query Processor` by asking an `AI Language Model` to extract the search keywords for this query that are suitable for a search engine.

Using these search keywords, a search query is sent to a full text search engine, which then returns hits. From this hit list, the first _n_ hits are downloaded and normalized (e.g. formatting removed).

Then these downloaded resources are concatenated and sent to the `AI Language Model` with a request to process them together, e.g., summarize them. The result of this processing is then presented to the user as the result of their request.