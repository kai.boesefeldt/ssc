/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
version = "0.0.1"

dependencies {
    implementation(project(":api"))
    implementation(project.properties["kotlinx-coroutines-core"].toString())
    implementation(project.properties["straightway.collections"].toString())
    implementation("org.scala-lang:scala-library:2.12.+")
    implementation("org.apache.spark:spark-mllib_2.12:3.3.2")
    implementation("com.johnsnowlabs.nlp:spark-nlp_2.12:4.3.2")
}