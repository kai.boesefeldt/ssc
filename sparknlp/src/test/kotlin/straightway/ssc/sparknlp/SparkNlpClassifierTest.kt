package straightway.ssc.sparknlp

import io.mockk.mockk
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import straightway.bdd.Given
import straightway.ssc.api.Environment
import straightway.ssc.api.InputCategory
import straightway.ssc.api.InputText

class SparkNlpClassifierTest {

    @Test
    @Disabled
    fun lalala() =
        Given {
            object {
                val sut = SparkNlpClassifier(
                    setOf(InputCategory("InputCategory")),
                )
            }
        } whenBlocking {
            val env = mockk<Environment>()
            with(sut) {
                env.run {
                    classify(
                        InputText(
                            InputCategory("inputCategory"),
                            "Dies ist ein Testtext. Der sollte als deutsch klassifiziert werden."
                        )
                    )
                }
            }
            true
        } then {

        }
}