package straightway.ssc.sparknlp

import com.johnsnowlabs.nlp.SparkNLP
import com.johnsnowlabs.nlp.pretrained.PretrainedPipeline
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import straightway.ssc.api.*
import java.nio.file.Files
import kotlin.io.path.div
import scala.collection.immutable.`Map$` as ScalaMap

class SparkNlpClassifier(
    private val inputCategories: Set<InputCategory>,
) : ActionClassifier {

    override fun supports(inputCategory: InputCategory) = inputCategory in inputCategories

    override suspend fun Environment.classify(input: InputText): Set<ActionClassification> {
        val tmp = withContext(Dispatchers.IO) {
            Files.createTempDirectory("SparkNLP")
        }
        val spark = SparkNLP.start(
            false,
            false,
            true,
            "memory",
            (tmp / "cache").toString(),
            (tmp / "log").toString(),
            (tmp / "cluster").toString(),
            ScalaMap.`MODULE$`.empty()
        )
        val scoringPipeline = PretrainedPipeline("analyze_sentiment", "de").lightModel();
        return setOf()
    }
}