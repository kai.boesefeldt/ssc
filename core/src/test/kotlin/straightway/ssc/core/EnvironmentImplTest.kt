package straightway.ssc.core

import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.CoroutineScope
import org.junit.jupiter.api.Test
import straightway.assertions.*
import straightway.bdd.Given
import straightway.collections.TypedMapKey
import straightway.collections.typedMapOf
import straightway.ssc.api.AI
import straightway.ssc.api.AIEngineClassification

class EnvironmentImplTest {

    private val test get() =
        Given {
            object {
                val configKey1 = TypedMapKey<Int>("configKey1")
                val configKey2 = TypedMapKey<String>("configKey2")
                val config = typedMapOf(
                    configKey1 to 83,
                    configKey2 to "World"
                )
                val ai1 = mockk<AI> {
                    every { classification } returns AIEngineClassification("ai1")
                }
                val ai2 = mockk<AI> {
                    every { classification } returns AIEngineClassification("ai2")
                }
                val ais = listOf(ai1, ai2)
                val coroutineScope = mockk<CoroutineScope>()
                val sut = coroutineScope.Environment(config, ais)
            }
        }

    @Test
    fun `topLevelCoroutineScope is as specified`() =
        test when_ {
            sut.topLevelCoroutineScope
        } then {
            assertThat(it is_ same as_ coroutineScope)
        }

    @Test
    fun `config is as specified`() =
        test when_ {
            sut.config
        } then {
            assertThat(it is_ equal to config)
        }

    @Test
    fun `session is initially empty`() =
        test when_ {
            sut.session
        } then {
            assertThat(it.journal is_ empty)
            assertThat(it.state is_ empty)
        }

    @Test
    fun `aiAccess allows access to the first AI`() =
        test when_ {
            sut.ai.get(AIEngineClassification("ai1"))
        } then {
            assertThat(it is_ same as_ ai1)
        }

    @Test
    fun `aiAccess allows access to the second AI`() =
        test when_ {
            sut.ai.get(AIEngineClassification("ai2"))
        } then {
            assertThat(it is_ same as_ ai2)
        }
}