package straightway.ssc.core

import kotlinx.coroutines.flow.flowOf
import org.junit.jupiter.api.Test
import straightway.assertions.*
import straightway.bdd.Given
import straightway.collections.TypedMapKey
import straightway.collections.mutableTypedMapOf
import straightway.ssc.api.*

class SessionImplTest {

    private companion object {
        val output = Output(
            OutputType("ot"),
            flowOf("Lalala"),
            InputText(InputCategory("ic"), "Lololo")
        )

        val key = TypedMapKey<Int>("key")
    }

    private val test
        get() = Given {
            SessionImpl()
        }

    @Test
    fun `initial session has empty journal`() =
        test when_ {
            journal
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `initial default constructed session has empty state`() =
        test when_ {
            state
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `constructor allows setting initial state`() =
        Given {
            SessionImpl(mutableTypedMapOf(key to 83))
        } when_ {
            state
        } then {
            assertThat(state is_ equal to values(key  to 83))
        }

    @Test
    fun `addToJournal adds journal entry`() {
        val journalEntry = JournalEntry(output.fromInput, output.type, listOf("Lalala"))
        test when_ {
            addToJournal(journalEntry)
        } then {
            assertThat(journal is_ equal to listOf(journalEntry))
        }
    }
}