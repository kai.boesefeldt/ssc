package straightway.ssc.core

import io.mockk.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Timeout
import org.junit.jupiter.api.assertThrows
import straightway.assertions.*
import straightway.bdd.Given
import straightway.collections.TypedMapKey
import straightway.collections.typedMapOf
import straightway.ssc.api.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

class SirisSillyCousinTest {

    @Nested
    inner class Construction {
        @Test
        fun `empty modules throws`() = runBlocking {
            val exception = assertThrows<IllegalArgumentException> {
                SirisSillyCousin(typedMapOf(), listOf())
            }
            assertThat(exception.message is_ equal to "No modules specified")
        }

        @Test
        fun `modules with missing dependencies throws`() = runBlocking {
            val exception = assertThrows<IllegalArgumentException> {
                SirisSillyCousin(
                    typedMapOf(),
                    listOf(
                        ProcessingModule(
                            ProcessingModuleId("id"),
                            dependsOn = setOf(ProcessingModuleId("notExistingId"))
                        )
                    )
                )
            }
            assertThat(exception.message is_ equal to "Module dependency error")
        }

        @Test
        fun `duplicate module ID throws`() = runBlocking {
            val exception = assertThrows<IllegalArgumentException> {
                SirisSillyCousin(
                    typedMapOf(),
                    listOf(
                        ProcessingModule(ProcessingModuleId("id")),
                        ProcessingModule(ProcessingModuleId("id"))
                    )
                )
            }
            assertThat(exception.message is_ equal to "Ambiguous module IDs")
        }

        @Test
        fun `duplicate AI classification throws`() = runBlocking {
            val ai = mockk<AI> {
                every { classification } returns AIEngineClassification("AI classification")
            }
            val exception = assertThrows<IllegalArgumentException> {
                SirisSillyCousin(
                    typedMapOf(),
                    listOf(
                        ProcessingModule(ProcessingModuleId("A"), ais = listOf(ai)),
                        ProcessingModule(ProcessingModuleId("B"), ais = listOf(ai))
                    )
                )
            }
            assertThat(exception.message is_ equal to "Ambiguous AI classifications")
        }

        @Test
        fun `order of modules is irrelevant with respect to dependencies`() = runBlocking {
            val modules = listOf(
                ProcessingModule(
                    ProcessingModuleId("A"),
                    dependsOn = setOf(ProcessingModuleId("B"))
                ),
                ProcessingModule(ProcessingModuleId("B"))
            )
            assertDoesNotThrow { SirisSillyCousin(typedMapOf(), modules) }
            assertDoesNotThrow { SirisSillyCousin(typedMapOf(), modules.reversed()) }
        }
    }

    @Nested
    inner class Run {
        @Test
        @Timeout(30, unit = TimeUnit.SECONDS)
        fun `input is properly passed through the processing pipeline`() =
            testProcessingInput {} then {
                coVerifyOrder {
                    with(environment.captured) {
                        inputProvider.run { receive(any()) }
                        actionClassifier.supports(input.category)
                        actionClassifier.run { classify(input) }
                        actionExecutor.supports(actionClassification)
                        actionExecutor.run { execute(input) }
                        outputConsumer.supports(outputType)
                        outputConsumer.run { receiveOutput(
                            coWithArg {
                                it shouldBe output
                            })
                        }
                    }
                }
            }

        @Test
        @Timeout(30, unit = TimeUnit.SECONDS)
        fun `config is passed on via environment`() =
            testProcessingInput {} then {
                assertThat(environment.captured.config is_ equal to config)
            }

        @Test
        @Timeout(30, unit = TimeUnit.SECONDS)
        fun `action output with type SameAsInput is passed to proper output consumer`() =
            testProcessingInput {
                output = Output(OutputType.SameAsInput, flowOf("output"), input)
            } then {
                coVerify {
                    with (any<Environment>()) {
                        outputConsumer.run {
                            receiveOutput(coWithArg {
                                it shouldBe output.copy(type = OutputType.from(input.category))
                            })
                        }
                    }
                }
            }

        @Test
        @Timeout(30, unit = TimeUnit.SECONDS)
        fun `input without matching action classifier is passed to unprocessedInput`() =
            testProcessingInput(setup = {
                every { actionClassifier.supports(any()) } returns false
            }) {
                unprocessedInput.receive()
            } then {
                assertThat(it is_ equal to input)
            }

        @Test
        @Timeout(30, unit = TimeUnit.SECONDS)
        fun `input without matching action is passed to unprocessedInput`() =
            testProcessingInput(setup = {
                every { actionExecutor.supports(any()) } returns false
            }) {
                unprocessedInput.receive()
            } then {
                assertThat(it is_ equal to input)
            }

        @Test
        @Timeout(30, unit = TimeUnit.SECONDS)
        fun `output without matching outputConsumer is passed to unprocessedOutput`() =
            testProcessingInput(setup = {
                every { outputConsumer.supports(any()) } returns false
            }) {
                unprocessedOutput.receive()
            } then {
                it shouldBe output
            }

        @Test
        @Timeout(30, unit = TimeUnit.SECONDS)
        fun `processing without output is not passed to unprocessedOutput`() =
            testProcessingInput(setup = {
                output = Output(outputType, flowOf(), input)
            }) {
                unprocessedOutput.tryReceive()
            } then {
                assertThat(it.isSuccess is_ false)
            }

        @Test
        @Timeout(30, unit = TimeUnit.SECONDS)
        fun `processing adds output to journal`() =
            testProcessingInput {} then {
                assertThat(environment.captured.session.journal is_ equal to
                    listOf(JournalEntry(input, output.type, outputStrings)))
            }

        @Test
        @Timeout(30, unit = TimeUnit.SECONDS)
        fun `partly processed output is only added partly to the journal`() =
            testProcessingInput {
                coEvery {
                    with(any<Environment>()) { outputConsumer.run { receiveOutput(any()) } }
                } coAnswers {
                    arg<Output>(1).text.first()
                }
            } then {
                assertThat(environment.captured.session.journal is_ equal to
                        listOf(JournalEntry(input, output.type, outputStrings.take(1))))
            }

        @Test
        @Timeout(30, unit = TimeUnit.SECONDS)
        fun `output passed to two consumers is only evaluated once`() {
            val consumedOutputs = Channel<String>(Channel.UNLIMITED)
            testProcessingInput {
                coEvery {
                    with(any<Environment>()) { outputConsumer.run { receiveOutput(any()) } }
                } coAnswers {
                    val output = arg<Output>(1).text.toList().joinToString(", ")
                    consumedOutputs.send(output)
                }
                val currOutputId = AtomicInteger(0)
                output = output.copy(
                    text = flow { emit("${currOutputId.incrementAndGet()}") }
                )
                module = module.copy(
                    outputConsumers = module.outputConsumers + module.outputConsumers
                )
            } then {
                runBlocking {
                    assertThat(consumedOutputs.receive() is_ equal to consumedOutputs.receive())
                }

            }
        }

        private fun testProcessingInput(setup: RunTestFixture.() -> Unit) =
            testProcessingInput(setup) {}

        private fun <T> testProcessingInput(
            setup: RunTestFixture.() -> Unit,
            test: suspend RunTestFixture.() -> T
        ) = runBlocking {
            Given {
                RunTestFixture(this)
            } while_ (setup) whenBlocking {
                coroutineScope {
                    launch {
                        sut.processInput(unprocessedInput, unprocessedOutput) {
                            sut.stop()
                        }
                    }
                    test()
                }
            }
        }

        private infix fun Output.shouldBe(other: Output) = runBlocking {
            assertThat(fromInput is_ equal to other.fromInput)
            assertThat(type is_ equal to other.type)
            assertThat(text.toList() is_ equal to other.text.toList())
        }
    }

    private class RunTestFixture(coroutineScope: CoroutineScope) : AutoCloseable {
        val inputCategory = InputCategory("InputCategory")
        val input = InputText(inputCategory, "Input")
        val environment = CapturingSlot<Environment>()
        val inputProvider = mockk<InputTextProvider> {
            coEvery { with(capture(environment)) { receive(any()) } } coAnswers {
                val inputChannel = arg<SendChannel<InputText>>(1)
                inputChannel.send(input)
            }
        }
        val actionClassification = ActionClassification("Action")
        val actionClassifier = mockk<ActionClassifier> {
            every { supports(any()) } returns true
            coEvery { with(any<Environment>()) { classify(any()) } } returns
                    setOf(actionClassification)
        }
        val outputType = OutputType("output")
        val outputStrings = listOf("output", "strings")
        var output: Output = Output(outputType, outputStrings.asFlow(), input)
        val actionExecutor = mockk<ActionExecutor> {
            every { supports(any()) } returns true
            coEvery { with(any<Environment>()) { execute(any()) } } answers { output }
        }
        val outputConsumer = mockk<OutputConsumer> {
            every { supports(any()) } returns true
            coEvery { with(any<Environment>()) { receiveOutput(any()) } } coAnswers {
                arg<Output>(1).text.collect {
                    println("output received '$it'")
                }
            }
        }
        val aiEngineClassification = AIEngineClassification("AI")
        val aiResponse = "AI Response"
        val ai = mockk<AI> {
            every { classification } returns aiEngineClassification
            coEvery { ask(any()) } returns aiResponse
        }
        var module = ProcessingModule(
            ProcessingModuleId("module"),
            inputTextProviders = listOf(inputProvider),
            actionClassifiers = listOf(actionClassifier),
            actionExecutors = listOf(actionExecutor),
            outputConsumers = listOf(outputConsumer),
            ais = listOf(ai)
        )
        val unprocessedInput = Channel<InputText>(Channel.UNLIMITED)
        val unprocessedOutput = Channel<Output>(Channel.UNLIMITED)
        val config = typedMapOf(TypedMapKey<Int>("configValue") to 83)
        val sut by lazy { coroutineScope.SirisSillyCousin(config, listOf(module)) }

        override fun close() {
            sut.close()
        }
    }
}