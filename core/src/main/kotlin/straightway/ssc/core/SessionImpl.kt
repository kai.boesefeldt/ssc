package straightway.ssc.core

import straightway.collections.MutableTypedMap
import straightway.collections.mutableTypedMapOf
import straightway.ssc.api.JournalEntry
import straightway.ssc.api.Session
import java.util.concurrent.ConcurrentLinkedDeque

class SessionImpl(override val state: MutableTypedMap = mutableTypedMapOf()) : Session {

    private val _journal = ConcurrentLinkedDeque<JournalEntry>()

    override val journal: List<JournalEntry> get() = _journal.toList()

    override fun addToJournal(journalEntry: JournalEntry) {
        _journal.add(journalEntry)
    }
}