package straightway.ssc.core

import kotlinx.coroutines.CoroutineScope
import straightway.collections.TypedMap
import straightway.ssc.api.*

fun CoroutineScope.Environment(config: TypedMap, ais: List<AI>): Environment =
    EnvironmentImpl(this, config, ais)

class EnvironmentImpl(
    override val topLevelCoroutineScope: CoroutineScope,
    override val config: TypedMap,
    private val ais: List<AI>
) : Environment {
    override val session: Session = SessionImpl()
    override val ai: AIAccess = AIAccess { classification ->
        ais.single { it.classification == classification }
    }
}