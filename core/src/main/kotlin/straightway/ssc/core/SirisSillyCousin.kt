package straightway.ssc.core

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import straightway.collections.TypedMap
import straightway.flow.wormFlowOf
import straightway.ssc.api.*

class SirisSillyCousin(
    private val coroutineScope: CoroutineScope,
    private val config: TypedMap,
    modules: List<ProcessingModule>
) : AutoCloseable {

    private val moduleInputs = modules.flatMap { it.inputTextProviders }
    private val actionClassifiers = modules.flatMap { it.actionClassifiers }
    private val actions = modules.flatMap { it.actionExecutors }
    private val outputConsumers = modules.flatMap { it.outputConsumers }
    private val ais = modules.flatMap { it.ais }
    private val inputChannel = Channel<InputText>(Channel.UNLIMITED)

    suspend fun processInput(
        unprocessedInput: SendChannel<InputText>? = null,
        unprocessedOutput: SendChannel<Output>? = null,
        onSingleInputHandled: suspend (InputText) -> Unit = {}
    ): Unit = coroutineScope {
        with(coroutineScope.Environment(config, ais)) {
            moduleInputs.forEach { inputTextProvider ->
                launch {
                    inputTextProvider.run { receive(inputChannel) }
                }
            }

            inputChannel.forEach {
                processInput(it, unprocessedInput, unprocessedOutput)
                onSingleInputHandled(it)
            }
        }
    }

    fun stop() {
        inputChannel.close()
    }

    override fun close() {
        runBlocking {
            stop()
        }
    }

    private suspend fun Environment.processInput(
        input: InputText,
        unprocessedInput: SendChannel<InputText>?,
        unprocessedOutput: SendChannel<Output>?
    ) {
        PipelineItem(this, input, actionClassifiers)
            .getActionClassifiersForInput()
            .classifyActions()
            .getActionExecutors()
            .ifEmptyPostTo(unprocessedInput)
            .executeActions()
            .translateOutputDestinationIfNecessary()
            .presentTheSameOutputToAllConsumers()
            .logInputAndOutputToJournal()
            .consumeOutput()
            .postUnconsumedOutputTo(unprocessedOutput)
    }

    private data class PipelineItem<T>(
        val environment: Environment,
        val input: InputText,
        val values: List<T>
    ) {
        fun <TOther> withValues(newValues: List<TOther>) =
            PipelineItem(environment, input, newValues)
    }

    private fun PipelineItem<ActionClassifier>.getActionClassifiersForInput() =
        withValues(values.filter { classifier -> classifier.supports(input.category) })

    private suspend fun PipelineItem<ActionClassifier>.classifyActions() =
        withValues(values.flatMap { classifier -> classifier.run { environment.classify(input) } })

    private fun PipelineItem<ActionClassification>.getActionExecutors() =
        withValues(actions.filter { action -> values.any { action.supports(it) } })

    private suspend fun PipelineItem<ActionExecutor>.ifEmptyPostTo(
        unprocessedInput: SendChannel<InputText>?
    ) = apply { if (values.isEmpty()) unprocessedInput?.sendOrIgnore(input) }

    private suspend fun PipelineItem<ActionExecutor>.executeActions() =
        withValues(values.map { action -> action.run { environment.execute(input) } })

    private fun PipelineItem<Output>.translateOutputDestinationIfNecessary() =
        withValues(values
            .map { output ->
                if (output.type == OutputType.SameAsInput)
                    Output(OutputType.from(input.category), output.text, input)
                else output
            }
        )
    private fun PipelineItem<Output>.presentTheSameOutputToAllConsumers() =
        withValues(
            values.map { output -> output.copy(text = coroutineScope.wormFlowOf(output.text)) }
        )

    private fun PipelineItem<Output>.logInputAndOutputToJournal() =
        withValues(
            values.map { output ->
                output.copy(
                    text = output.text.addEntriesToJournal(environment.session) {
                        JournalEntry(input, output.type, it)
                    }
                )
            }
        )

    private suspend fun PipelineItem<Output>.consumeOutput() =
        withValues(values.filter { !consume(it) } )

    private suspend fun PipelineItem<Output>.postUnconsumedOutputTo(
        unprocessedOutput: SendChannel<Output>?
    ) {
        if (unprocessedOutput !== null)
            values.forEach { output -> unprocessedOutput.sendOrIgnore(output) }
    }

    private suspend fun PipelineItem<*>.consume(output: Output): Boolean {
        val matchingConsumers = outputConsumers.filter { consumer ->
            consumer.supports(output.type)
        }
        matchingConsumers.forEach { consumer ->
            consumer.run { environment.receiveOutput(output) }
        }
        return matchingConsumers.any()
    }

    private fun Flow<String>.addEntriesToJournal(
        session: Session,
        journalEntry: (List<String>) -> JournalEntry
    ): Flow<String> {
        val outputStrings = mutableListOf<String>()
        return onEach { outputStrings.add(it) }
            .onCompletion { session.addToJournal(journalEntry(outputStrings.toList())) }
    }

    private suspend fun <T> SendChannel<T>.sendOrIgnore(item: T) {
        try {
            send(item)
        } catch (_: ClosedSendChannelException) {
            /* ignore */
        }
    }

    private suspend fun <T> ReceiveChannel<T>.forEach(block: suspend (T) -> Unit) {
        while (true)
            try {
                block(receive())
            } catch (_: ClosedReceiveChannelException) {
                break
            }
    }

    init {
        require(modules.any()) {
            "No modules specified"
        }
        require(modules.map { it.id }.toSet().size == modules.size) {
            "Ambiguous module IDs"
        }
        require(ais.map { it.classification }.toSet().size == ais.size) {
            "Ambiguous AI classifications"
        }
        checkDependenciesOf(toCheck = modules, passed = listOf())
    }

    private fun checkDependenciesOf(
        toCheck: List<ProcessingModule>,
        passed: List<ProcessingModule>
    ) {
        if (toCheck.isEmpty())
            return

        val moduleWithSatisfiedDependencies = toCheck.firstOrNull { moduleToCheck ->
            moduleToCheck.dependsOn.all { wantedModuleId ->
                passed.any { passedModuleId -> passedModuleId.id == wantedModuleId }
            }
        }
        requireNotNull(moduleWithSatisfiedDependencies) { "Module dependency error" }

        checkDependenciesOf(
            toCheck - moduleWithSatisfiedDependencies,
            passed + moduleWithSatisfiedDependencies
        )
    }
}

fun CoroutineScope.SirisSillyCousin(config: TypedMap, modules: List<ProcessingModule>) =
    SirisSillyCousin(this, config, modules)
