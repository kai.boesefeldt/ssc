package straightway.ssc.console

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import straightway.collections.typedMapOf
import straightway.ssc.api.InputText
import straightway.ssc.api.Output
import straightway.ssc.api.ProcessingModule
import straightway.ssc.api.ProcessingModuleId
import straightway.ssc.base.BASE_MODULE
import straightway.ssc.console.io.ConsoleIO
import straightway.ssc.core.SirisSillyCousin

suspend fun main() {
    coroutineScope {
        val consoleIO = ConsoleIO()
        val modules = listOf(
            BASE_MODULE,
            ProcessingModule(
                id = ProcessingModuleId("ssc.console"),
                dependsOn = setOf(BASE_MODULE.id),
                inputTextProviders = listOf(consoleIO),
                outputConsumers = listOf(consoleIO)
            )
        )
        val config = typedMapOf()

        SirisSillyCousin(config, modules).use { ssc ->
            val unprocessedInput = Channel<InputText>(Channel.UNLIMITED)
            val unprocessedOutput = Channel<Output>(Channel.UNLIMITED)
            continuouslyCopyContents("unprocessed input", unprocessedInput, consoleIO.error)
            continuouslyCopyContents("unprocessed output", unprocessedOutput, consoleIO.error)

            println("Hi! I'm Siri's Silly Cousin!")
            ssc.processInput(unprocessedInput, unprocessedOutput)

            println("Bye!")

            unprocessedInput.close()
            unprocessedOutput.close()
        }
    }
}

private fun CoroutineScope.continuouslyCopyContents(
    messagePrefix: String,
    unprocessedInput: ReceiveChannel<*>,
    sendChannel: SendChannel<String>
) = launch {
    try {
        while (true)
            sendChannel.send("$messagePrefix: ${unprocessedInput.receive()}")
    } catch (_: ClosedReceiveChannelException) {
        // Ignore
    }
}