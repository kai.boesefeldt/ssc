package straightway.ssc.console.io

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import straightway.ssc.api.*
import straightway.ssc.base.InputCategories

class ConsoleIO : InputTextProvider, OutputConsumer {

    private val outputChannel = Channel<String>(Channel.UNLIMITED)
    private val errorChannel = Channel<String>(Channel.UNLIMITED)

    val error: SendChannel<String> get() = errorChannel

    override suspend fun Environment.receive(input: SendChannel<InputText>) {
        coroutineScope {
            outputToConsole()
            errorToConsole()
            retrieveInput(input)

            input.close()
            outputChannel.close()
            errorChannel.close()
        }
    }

    override fun supports(outputType: OutputType) = outputType == OUTPUT_TYPE

    override suspend fun Environment.receiveOutput(output: Output) {
        output.text.collect { outputChannel.send(it) }
        outputChannel.send("\n")
    }

    companion object {
        val OUTPUT_TYPE = OutputType.from(InputCategories.CHAT)
    }

    private suspend fun retrieveInput(input: SendChannel<InputText>) {
        while (true) {
            val inputText = readlnOrNull()
            inputText?.also { input.send(InputText(InputCategories.CHAT, it)) } ?: break
        }
    }

    private fun CoroutineScope.outputToConsole() {
        launch {
            try {
                while (true) {
                    print(outputChannel.receive())
                }
            } catch (_: ClosedReceiveChannelException) {
                // Ignore
            }
        }
    }

    private fun CoroutineScope.errorToConsole() {
        launch {
            try {
                while (true) {
                    println(errorChannel.receive())
                    println()
                }
            } catch (_: ClosedReceiveChannelException) {
                // Ignore
            }
        }
    }
}