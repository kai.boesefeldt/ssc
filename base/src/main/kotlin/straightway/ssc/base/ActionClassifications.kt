package straightway.ssc.base

import straightway.ssc.api.ActionClassification

object ActionClassifications {
    val ANSWER_CASUAL_CHAT =
        ActionClassification("straightway.ssc.base.ActionClassifications.ANSWER_CASUAL_CHAT")
}