package straightway.ssc.base

import straightway.ssc.api.ProcessingModule
import straightway.ssc.api.ProcessingModuleId

val BASE_MODULE = ProcessingModule(
    ProcessingModuleId("straightway.ssc.base"),
    actionClassifiers = listOf(CasualChatClassifier()),
    actionExecutors = listOf(CasualChatter())
)