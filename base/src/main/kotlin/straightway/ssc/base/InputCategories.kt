package straightway.ssc.base

import straightway.ssc.api.InputCategory

object InputCategories {
    val CHAT = InputCategory("straightway.ssc.base.InputCategories.CHAT")
}