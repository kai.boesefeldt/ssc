package straightway.ssc.base

import straightway.ssc.api.*

class CasualChatClassifier : ActionClassifier {
    override fun supports(inputCategory: InputCategory) =
        inputCategory == InputCategories.CHAT

    override suspend fun Environment.classify(input: InputText) =
        setOf(ActionClassifications.ANSWER_CASUAL_CHAT)
}