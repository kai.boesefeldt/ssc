package straightway.ssc.base

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import straightway.ssc.api.*
import kotlin.time.Duration.Companion.milliseconds

class CasualChatter : ActionExecutor {

    override fun supports(actionClassification: ActionClassification) =
        actionClassification == ActionClassifications.ANSWER_CASUAL_CHAT

    override suspend fun Environment.execute(input: InputText): Output {
        val c = Channel<String>(Channel.UNLIMITED)
        topLevelCoroutineScope.launch {
            c.send("Answer ")
            delay(500.milliseconds)
            c.send("to ")
            delay(500.milliseconds)
            c.send(input.text)
            delay(500.milliseconds)
            c.close()
        }
        return Output(
            OutputType.from(input.category),
            c.consumeAsFlow(),
            input
        )
    }
}