package straightway.ssc.opennlp

import opennlp.tools.doccat.DocumentCategorizerME
import opennlp.tools.langdetect.LanguageDetectorME
import opennlp.tools.lemmatizer.LemmatizerME
import opennlp.tools.postag.POSTaggerME
import opennlp.tools.sentdetect.SentenceDetectorME
import opennlp.tools.tokenize.TokenizerME
import straightway.ssc.api.*

class OpenNlpClassifier(
    private val inputCategories: Set<InputCategory>,
    private val model: OpenNlpModel,
    private val minProbability: Double
) : ActionClassifier {

    override fun supports(inputCategory: InputCategory) = inputCategory in inputCategories

    override suspend fun Environment.classify(input: InputText): Set<ActionClassification> {
        val languageDetector = LanguageDetectorME(model.languageDetectorModel)
        val language = languageDetector.predictLanguage(input.text)
        val languageModel = model.languageModel[language.lang]!!
        val sentences = languageModel.sentenceModel?.let { sentenceModel ->
            val sentenceDetector = SentenceDetectorME(sentenceModel)
            sentenceDetector.sentDetect(input.text)
        } ?: arrayOf(input.text)
        val tokenSentences = languageModel.tokenizerModel?.let { tokenizerModel ->
            val tokenizer = TokenizerME(tokenizerModel)
            sentences.map { sentence ->
                tokenizer.tokenize(sentence)
            }
        } ?: sentences.map { it.split("\\b|(?=[.,!?])".toRegex()).toTypedArray() }
        val lemmatizedSentences = languageModel.posModel?.let { posModel ->
            languageModel.lemmatizerModel?.let { lemmatizerModel ->
                val posTagger = POSTaggerME(posModel)
                val posTags = tokenSentences.map { sentence ->
                    posTagger.tag(sentence)
                }

                val lemmatizer = LemmatizerME(lemmatizerModel)
                tokenSentences.zip(posTags).map { tokensAndPosTags ->
                    val (tokens, posTags) = tokensAndPosTags
                    lemmatizer.lemmatize(tokens, posTags)
                }
            }
        } ?: tokenSentences

        val categorizer = DocumentCategorizerME(languageModel.classifierModel)
        val categories = categorizer.sortedScoreMap(
            lemmatizedSentences.flatMap { it.toList() }.toTypedArray()
        )

        return categories
            .filter { minProbability <= it.key}
            .flatMap { it.value.map { ActionClassification(it) } }
            .toSet()
    }
}