package straightway.ssc.opennlp

import opennlp.tools.langdetect.LanguageDetectorModel

data class OpenNlpModel(
    val languageDetectorModel: LanguageDetectorModel,
    val languageModel: Map<String, OpenNlpLanguageSpecificModel>
)