package straightway.ssc.opennlp

import opennlp.tools.doccat.DoccatModel
import opennlp.tools.lemmatizer.LemmatizerModel
import opennlp.tools.postag.POSModel
import opennlp.tools.sentdetect.SentenceModel
import opennlp.tools.tokenize.TokenizerModel

data class OpenNlpLanguageSpecificModel(
    val sentenceModel: SentenceModel?,
    val tokenizerModel: TokenizerModel?,
    val posModel: POSModel?,
    val lemmatizerModel: LemmatizerModel?,
    val classifierModel: DoccatModel,
)