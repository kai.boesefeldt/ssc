package straightway.ssc.opennlp

import io.mockk.mockk
import kotlinx.coroutines.coroutineScope
import opennlp.tools.doccat.DoccatModel
import opennlp.tools.langdetect.LanguageDetectorModel
import opennlp.tools.postag.POSModel
import opennlp.tools.sentdetect.SentenceModel
import opennlp.tools.tokenize.TokenizerModel
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import straightway.bdd.Given
import straightway.collections.typedMapOf
import straightway.ssc.api.Environment
import straightway.ssc.api.InputCategory
import straightway.ssc.api.InputText
import straightway.ssc.api.Session
import java.net.URL

class OpenNlpClassifierTest {

    @Test
    @Disabled
    fun lalala() =
        Given {
            object {
                val model = OpenNlpModel(
                    languageDetectorModel = LanguageDetectorModel(URL("https://dlcdn.apache.org/opennlp/models/langdetect/1.8.3/langdetect-183.bin")),
                    languageModel = mapOf("deu" to OpenNlpLanguageSpecificModel(
                        sentenceModel = SentenceModel(URL("https://dlcdn.apache.org/opennlp/models/ud-models-1.0/opennlp-de-ud-gsd-sentence-1.0-1.9.3.bin")),
                        tokenizerModel = TokenizerModel(URL("https://dlcdn.apache.org/opennlp/models/ud-models-1.0/opennlp-de-ud-gsd-tokens-1.0-1.9.3.bin")),
                        posModel = POSModel(URL("https://dlcdn.apache.org/opennlp/models/ud-models-1.0/opennlp-de-ud-gsd-pos-1.0-1.9.3.bin")),
                        lemmatizerModel = mockk(),
                        classifierModel = mockk()
                    ))
                )
                val sut = OpenNlpClassifier(
                    setOf(InputCategory("InputCategory")),
                    model,
                    minProbability = 0.85
                )
            }
        } whenBlocking {
            val env = mockk<Environment>()
            with(sut) {
                env.run {
                    classify(
                        InputText(
                            InputCategory("inputCategory"),
                            "Dies ist ein Testtext. Der sollte als deutsch klassifiziert werden."
                        )
                    )
                }
            }
            true
        } then {

        }
}