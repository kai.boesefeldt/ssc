package straightway.ssc.api

/**
 * Execute an action resulting from an input classification.
 */
interface ActionExecutor {

    /**
     * Determines if the given action classification is supported by the action executor.
     */
    fun supports(actionClassification: ActionClassification): Boolean

    /**
     * Execute an action after classifying input for that action.
     * @param input The text input being the basis for the action classification.
     * @return May produce an output text for an OutputTextConsumer. The output may contain
     * a null text, which means that no output has been produced.
     */
    suspend fun Environment.execute(input: InputText): Output
}
