package straightway.ssc.api

import kotlinx.coroutines.flow.Flow

data class Output(val type: OutputType, val text: Flow<String>, val fromInput: InputText)