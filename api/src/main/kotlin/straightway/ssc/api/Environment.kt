package straightway.ssc.api

import kotlinx.coroutines.CoroutineScope
import straightway.collections.TypedMap

interface Environment {
    val topLevelCoroutineScope: CoroutineScope
    val config: TypedMap
    val session: Session
    val ai: AIAccess
}