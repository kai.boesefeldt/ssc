package straightway.ssc.api

import straightway.collections.MutableTypedMap

interface Session {
    val journal: List<JournalEntry>

    fun addToJournal(journalEntry: JournalEntry)

    val state: MutableTypedMap
}

