package straightway.ssc.api

data class OutputType(val id: String) {

    companion object {
        val SameAsInput = OutputType("SameAsInput")
        fun from(inputCategory: InputCategory) = OutputType("${inputCategory.id}->Output")
    }
}