package straightway.ssc.api

/**
 * Classify an input and yield an action classification ID.
 */
interface ActionClassifier {

    fun supports(inputCategory: InputCategory): Boolean

    suspend fun Environment.classify(input: InputText): Set<ActionClassification>
}