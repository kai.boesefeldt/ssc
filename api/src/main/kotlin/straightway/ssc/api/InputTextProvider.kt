package straightway.ssc.api

import kotlinx.coroutines.channels.SendChannel

/**
 * Provides textual input from some input source.
 */
interface InputTextProvider {

    /**
     * Receive input and send it via the input channel.
     * Runs until no more input can be received or stop is called.
     * @param input The channel on which the received input is transmitted into the system.
     */
    suspend fun Environment.receive(input: SendChannel<InputText>)
}