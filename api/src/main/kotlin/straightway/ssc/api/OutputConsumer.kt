package straightway.ssc.api

/**
 * Consumes output coming from an action executor.
 */
interface OutputConsumer {

    /**
     * Determines if the given output type is supported by the consumer.
     */
    fun supports(outputType: OutputType): Boolean

    /**
     * Receives the output to consume.
     * @param output A string representation of the output.
     */
    suspend fun Environment.receiveOutput(output: Output)
}
