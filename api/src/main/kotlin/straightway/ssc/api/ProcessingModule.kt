package straightway.ssc.api

data class ProcessingModule(
    val id: ProcessingModuleId,
    val dependsOn: Set<ProcessingModuleId> = setOf(),
    val inputTextProviders: List<InputTextProvider> = listOf(),
    val actionClassifiers: List<ActionClassifier> = listOf(),
    val actionExecutors: List<ActionExecutor> = listOf(),
    val outputConsumers: List<OutputConsumer> = listOf(),
    val ais: List<AI> = listOf()
)