package straightway.ssc.api

fun interface AIAccess {
    fun get(classification: AIEngineClassification): AI
}