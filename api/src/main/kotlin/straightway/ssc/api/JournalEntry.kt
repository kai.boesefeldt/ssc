package straightway.ssc.api

data class JournalEntry(
    val input: InputText,
    val outputType: OutputType,
    val output: List<String>,
    val cookie: Any? = null)