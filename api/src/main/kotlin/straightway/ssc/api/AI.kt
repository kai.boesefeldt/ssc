package straightway.ssc.api

/**
 * Interact with an artificial intelligence.
 */
interface AI {

    val classification: AIEngineClassification

    /**
     * Pass a question or request and get a reply.
     * @param input The question or request.
     * @return The reply.
     */
    suspend fun ask(input: String): String
}

