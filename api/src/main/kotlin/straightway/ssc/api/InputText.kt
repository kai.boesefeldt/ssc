package straightway.ssc.api

data class InputText(val category: InputCategory, val text: String)